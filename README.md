# README #

This application is a barebone DOS prompt program, which interfaces with MCC DAQ, USB-1616HS-2 more specifically. 
Read the real time acquisition and save into a file in D: drive. It's only for data logger purpose.

### How do I get set up? ###

Users have to install MCC DAQ driver, InstaCal, to run the program successfully. 

InstaCal: http://www.mccdaq.com/daq-software/instacal.aspx