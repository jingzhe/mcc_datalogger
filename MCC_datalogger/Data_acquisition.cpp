/***************************************************************************
*
*   Data_acquisition.cpp - It's a daq device independent definition.
*                          Defines the basic precedure how to reteive and organize data
*
***************************************************************************/
/*-------------------------------------------------------------------------*/
#include "Data_acquisition.h"

static DWORD timer_1000ms = 0;
#define ENCODER_PPR 40 // pulses per revolution.
/*-------------------------------------------------------------------------*/
//
//   running task
//
//
DWORD WINAPI D_acq_thread(LPVOID lpParameter)
{
	Data_acquisition* myacq = (Data_acquisition*)lpParameter;
	timer_1000ms = GetTickCount() + 1000;
	unsigned int encoder_cnt = 0;
	while (1)
	{
		if (myacq->needed_acq)
		{
			myacq->collect();
		}
	}

	return 0;
}
/*-------------------------------------------------------------------------*/
//
//   calculate speed every 1s
//
//
float Data_acquisition::cal_spd(unsigned int &cnt)
{
	current_spd = cnt / ENCODER_PPR;
	return current_spd;
}
/*-------------------------------------------------------------------------*/
//
//   Constructor
//
//
Data_acquisition::Data_acquisition(const int &_data_size, unsigned int chn_num, 
	const int &sampling_rate, void(*cb)(vector<double>&, vector<double>&))
{
	data_size = _data_size;
	scanRate = sampling_rate;
	Channel_num = chn_num; // includes 2 ad inputs. encoder count is retieved from command response.

	if (!InitializeCriticalSectionAndSpinCount(&CriticalSection,
		0x00000400))
		return;
	add_data_cb = cb;

	needed_acq = false;

	DWORD myThreadID;
	HANDLE myHandle = CreateThread(0, 0, D_acq_thread, this, 0, &myThreadID);
}
/*-------------------------------------------------------------------------*/
//
//   Destructor
//
//
Data_acquisition::~Data_acquisition()
{
	DeleteCriticalSection(&CriticalSection);
}
/*-------------------------------------------------------------------------*/
//
//   Triggered by GUI
//
//
bool Data_acquisition::stop()
{
	EnterCriticalSection(&CriticalSection);
	needed_acq = false;
	channel1.clear();
	channel1.reserve(data_size);
	channel2.clear();
	channel1.reserve(data_size);
	LeaveCriticalSection(&CriticalSection);
	return true;
}
/*-------------------------------------------------------------------------*/
//
//   Triggered by GUI
//
//
bool Data_acquisition::start()
{
	last_processed_idx = -1;
	needed_acq = true;
	return true;
}

/*-------------------------------------------------------------------------*/
//
//   When data is ready, call the callback function to dump the data
//
//
void Data_acquisition::add_data()
{
	add_data_cb(channel1, channel2);
	channel1.clear();
	channel2.clear();
	channel1.reserve(data_size);
	channel2.reserve(data_size);
	isready = false;
}
/*-------------------------------------------------------------------------*/
//
//   Work with daq device to retreive data from the stream.
//   This function only defines the precedure, but a sub class defined a pratical daq device
//   have to implement the functions
//
//
void Data_acquisition::collect()
{
	//if (current_spd == 0)
	//	return;

	Status = retreive_data();

	if (Status == 0)
	{
		EnterCriticalSection(&CriticalSection);

		transfer_data();

		add_data();
		EnterCriticalSection(&CriticalSection);
	}
}