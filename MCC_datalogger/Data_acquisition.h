#pragma once
#include <windows.h>
#include <vector>
using namespace std;
class Data_acquisition
{
protected:
	CRITICAL_SECTION CriticalSection;

	long last_processed_idx;

	unsigned int Channel_num;
	short Status;
	long data_size;
	float scanRate;
	bool isready;
	unsigned int CurIndex;
	vector<double> channel1;
	vector<double> channel2;
	float current_spd;
	void (*add_data_cb)(vector<double> &, vector<double> &);
	unsigned char *rawData;

	virtual short retreive_data() = 0;
	virtual double convert_unit(unsigned int &j) = 0;
	virtual void transfer_data()=0;
public:
	Data_acquisition(const int &_data_size, unsigned int chn_num, const int &, void(*cb)(vector<double>&, vector<double>&));
	~Data_acquisition();
	bool needed_acq;
	void add_data();
	void collect();
	virtual bool stop()  = 0;
	virtual bool start() = 0;
	float cal_spd(unsigned int&);

};