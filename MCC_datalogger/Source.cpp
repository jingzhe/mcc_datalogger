
/* Include files */
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <iostream>
#include <iomanip>

#include <vector>
#include <fstream>
#include <sstream>
#include "MCC_DAQ\cbw.h"
#include "Data_acquisition.h"
#include "MCC_DAQ/USB_1616hs.h"
using namespace std;
#define uint unsigned int

#define NUMCHANS   4
#define NUMPOINTS 10


//save points into a file with a comma as the splitter.
void save_to_file(string &save_to_fname, vector<double> &x, vector<double> &y)
{
	ofstream txt;
	txt.open(save_to_fname);
	for (unsigned int i = 0; i < x.size() && i < y.size(); i++)
		txt << x[i] << "," << y[i] << endl;
	txt.close();
}

#define NUMCHANS  4
#define NUMPOINTS 10000
const char LABJACK_T7_IP_ADDR[] = "10.32.4.124"; //Set your IP Addresses here, or set it using the first argument when running the program.

// Sampling frequency
float sampling_freq = 10000; // Hz
														 // This for calculate time eclipsed during the recording
float TIME_INTERVAL = 1 / sampling_freq;//

// Location for data logger files
const char* const LOG_LOC = "c:\\";
string chn1_str;


void Log(float a, const float &interval, vector<double> &b, const char* loc)
{
	ofstream txt;
	txt.open(loc, ios::app);
	for (uint i = 0; i < b.size(); i++)
	{
		txt << a << "\t" << b[i] << endl;
		a += interval;
	}
	txt.close();
}


double Elapsed_time=0;
void Acquired_data_cb(vector<double> &ch1, vector<double> &ch2)
{
	Log(Elapsed_time, TIME_INTERVAL, ch1, chn1_str.c_str());

	Elapsed_time += TIME_INTERVAL * ch1.size();
}

/*
 *	Check if a file is existed.
 */
static BOOL fexists(const char *filename)
{
	ifstream ifile(filename);
	return !ifile.fail();
}

/*
 *	Name a file as "file_yymmdd_hhmmss.txt"
 */
static void define_filename()
{
	stringstream chn1_url;
	SYSTEMTIME sys_time;
	GetSystemTime(&sys_time);
	chn1_url << LOG_LOC << "file_" << sys_time.wYear
		<< setfill('0') << setw(2) << sys_time.wMonth
		<< setfill('0') << setw(2) << sys_time.wDay << "_"
		<< setfill('0') << setw(2) << sys_time.wHour
		<< setfill('0') << setw(2) << sys_time.wMinute
		<< setfill('0') << setw(2) << sys_time.wSecond << ".txt";

	chn1_url >> chn1_str;
}


void main()
{
	uint chn_num = 1;
	const int data_size= 10000;
	MCC_USB_1616HS_2_DAQ_class daq(data_size, chn_num, sampling_freq, &Acquired_data_cb, LABJACK_T7_IP_ADDR);
	char input;

	printf("DSF Data Acquisition Tool\n");
	printf("-------------------------------\n");
	printf(" r: start DAQ.\n p: Pause.\n x: Exit...\n");

	while(1)
	{ 
		std::cin >> input;
		switch (input )
		{
		case 'r':
			define_filename();
			if( daq.start() )
				printf("DAQ started. \n");
			break;
		case 'p':
			if( daq.stop() )
				printf("DAQ paused. \n");
			break;
		case 'x':
			daq.stop();
			exit(0);
		}
	}
}

