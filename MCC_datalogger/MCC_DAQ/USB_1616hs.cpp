#include "USB_1616hs.h"
#include <conio.h>
#include <stdio.h>
#define NUMPOINTS 100000
MCC_USB_1616HS_2_DAQ_class::MCC_USB_1616HS_2_DAQ_class(const int &_data_size, unsigned int  chn_num,
	const int &sampling_rate, void(*cb)(vector<double>&, vector<double>&), const char* ip)
	: Data_acquisition(_data_size, chn_num, sampling_rate, *cb)
{
	/* Declare UL Revision Level */
	ULStat = cbDeclareRevision(&RevLevel);


	cbErrHandling(PRINTALL, DONTSTOP);

	/* load the arrays with values */
	ChanArray[0] = 0;
	ChanTypeArray[0] = ANALOG;
	GainArray[0] = BIP5VOLTS;

	ChanCount = chn_num;
	PreTrigCount = 0;
	TotalCount = _data_size * chn_num;
	Rate = sampling_rate;								             /* sampling rate (samples per second) */
	Options = CONVERTDATA + BACKGROUND + CONTINUOUS;         /* data collection options */

	EngUnits = (double*)malloc(sizeof(double));

	past_index = -1;
}


short MCC_USB_1616HS_2_DAQ_class::retreive_data()
{

		/* Check the status of the current background operation
		Parameters:
		BoardNum  :the number used by CB.CFG to describe this board
		Status    :current status of the operation (IDLE or RUNNING)
		CurCount  :current number of samples collected
		CurIndex  :index to the last data value transferred
		FunctionType: A/D operation (DAQIFUNCTION)*/
		ULStat = cbGetStatus(BoardNum, &Status, &CurCount, &CurIndex, DAQIFUNCTION);
		if (Status == RUNNING)
		{
			if (CurIndex != past_index)
			{
				return 0;
			}
			else 
				return 1;
		}
		else
			return 1;
}
double MCC_USB_1616HS_2_DAQ_class::convert_unit(unsigned int & j)
{
	
	cbToEngUnits32(BoardNum, GainArray[0], ADData[j], EngUnits);
	return *EngUnits;
}

void MCC_USB_1616HS_2_DAQ_class::transfer_data()
{
	float volt;
	int count = 0;
	short channel_idx = 0;
	long saved_index = 0;
	if (past_index > CurIndex)
	{
		saved_index = CurIndex;
		CurIndex = TotalCount;
	}
	for (unsigned int I = past_index; I < CurIndex; I++)   /* loop through the values & print */
	{
		channel_idx = I % Channel_num;

		switch (channel_idx)
		{
		case 0:
			volt = convert_unit(I);
			channel1.push_back(volt);
			break;
		case 1:
			//volt = convert_unit(I);
			//channel2.push_back(volt);
			break;
		default:
			break;
		}
	}
	if (saved_index != 0)
		past_index = saved_index;
	else
		past_index = CurIndex;

}

bool MCC_USB_1616HS_2_DAQ_class::stop()
{	
	/* The BACKGROUND operation must be explicitly stopped
	Parameters:
	BoardNum    :the number used by CB.CFG to describe this board
	FunctionType: A/D operation (DAQIFUNCTION)*/
	if (Status == RUNNING)
	{
		ULStat = cbStopBackground(BoardNum, DAQIFUNCTION);
	}
	else
		return false;

	if (ADData != NULL)
	{
		cbWinBufFree(ADData);
		ADData = NULL;
	}
	else 
		return false;

	return true;

}

bool MCC_USB_1616HS_2_DAQ_class::start()
{
	stop();

	needed_acq = 1;

	ADData = (WORD*)cbWinBufAlloc(TotalCount);
	if (!ADData)    /* Make sure it is a valid pointer */
	{
		printf("\nout of memory\n");
		exit(1);
	}

	ULStat = cbDaqInScan(BoardNum, ChanArray, ChanTypeArray,
		GainArray, ChanCount, &Rate, &PreTrigCount, &TotalCount, ADData, Options);

	if (ULStat == NOERRORS)
		Status = RUNNING;
	else
		return false;

	return true;
}
