#include "../Data_acquisition.h"
#include "cbw.h"

#define NUMCHANS 1

class MCC_USB_1616HS_2_DAQ_class : public Data_acquisition
{
public:
	MCC_USB_1616HS_2_DAQ_class(const int &_data_size, unsigned int chn_num,
		const int &sampling_rate, void(*cb)(vector<double>&, vector<double>&), const char* ip);

	short retreive_data();
	double convert_unit(unsigned int &j);

	void transfer_data();
	bool stop();
	bool start();


private:
	double *EngUnits;
	long past_index;
	int Row, Col;
	int  BoardNum = 0;
	int Options;
	long PreTrigCount, TotalCount, Rate, ChanCount;
	short ChanArray[NUMCHANS];
	short ChanTypeArray[NUMCHANS];
	short GainArray[NUMCHANS];
	int ULStat = 0;
	short Status = IDLE;
	long CurCount;
	long CurIndex, DataIndex;
	int PortNum, Direction, CounterNum;
	WORD *ADData;
	float    RevLevel = (float)CURRENTREVNUM;

};